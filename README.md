Ansible playbook which deploying test environment in localhost docker containers.
There are such services running:

MariaDB (Ubuntu)
NGINX+Apache+php (2x - CentOS and Ubuntu)
Haproxy (Ubuntu)

Were used this docker images:

https://hub.docker.com/r/centos/systemd/
https://hub.docker.com/r/phusion/baseimage/

Requirements:

ansible
docker-ce
docker-py
python 2.x

Some issues:

1. In case with error "Failed to connect to the host via ssh: Permission denied (publickey,password)"
Please, try registering the private key to your keychain:

      ssh-agent bash
      ssh-add <path to private key>

2. There are hard specified path in playbook ~/ansible_home_work/roles/docker/tasks/main.yml (line 18 & 25)

      path: ~/ansible_home_work

Supposed that project folder "ansible_home_work" will be in home directory of your user.

3. In case of running playbook from root user of your OS, please comment this lines in
ansible_home_work/inventories/production/group_vars/all.yml

     #ansible_become: yes
     #ansible_become_method: sudo

Run playbook

To run playbook type this:

     ~/ansible_home_work# ansible-playbook site.yml

To see the result, please use curl after deploying (Haproxy IP):

     # curl 10.0.100.2
     Connected to DB successfully from container <container_id>
     # curl 10.0.100.2
     Connected to DB successfully from container <container_id>
